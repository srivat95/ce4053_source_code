#include <os.h>
#include <limits.h>
#include <stdlib.h>

/*****************************************Initialize Skip List*******************************************/
void          OS_SkipListInit           (void)
{   OS_ERR err;   
    OS_SKIPLIST_NODE *header = OSMemGet(&OS_SkipListPartition,&err);
    OS_SKIPLIST *p_skiplist=&OS_SkipList;
    p_skiplist->header = header;
    header->period =INT_MAX;
    header->next_time = INT_MAX;
    header->p_tcb = (OS_TCB *) OSMemGet(&OS_SkipListPartition2,&err);
    CPU_INT08U i;
    for (i = 0; i <= OS_SKIPLIST_MAX_LEVEL; i++){
        header->forward[i] = (OS_SKIPLIST_NODE *) 0;
        header->forward[i]-> period = INT_MAX;
        header->forward[i]->next_time = INT_MAX;
        
    }
    
    p_skiplist->level = 1;
    p_skiplist->size  = 0;
    
}
/****************************************Randomize skip list level**************************************/
CPU_INT08U    OS_SkipListRandLevel      (void)
{ CPU_INT08U level = 1;
    while (rand() < RAND_MAX/2 && level < OS_SKIPLIST_MAX_LEVEL)
        level++;
    return level;
}

/****************************************Insert task into skip list ************************************/
void          OS_SkipListInsert         (OS_TCB *p_tcb, OS_TICK period)
{   OS_ERR err;
    OS_SKIPLIST_NODE *update[OS_SKIPLIST_MAX_LEVEL+1];
    CPU_INT08U i, level;
    OS_SKIPLIST *p_skiplist = &OS_SkipList;
    OS_SKIPLIST_NODE *x = p_skiplist->header;

    for (i = p_skiplist->level; i >= 1; i--) {
        while ((x->forward[i]!= (OS_SKIPLIST_NODE *) 0)&&(x->forward[i]->period < period) )
            x = x->forward[i];
        update[i] = x;
    }
    x = x->forward[1];

    level = OS_SkipListRandLevel();
    if (level > p_skiplist->level) {
        for (i = p_skiplist->level+1; i <= level; i++) {
            update[i] = p_skiplist->header;
        }
        p_skiplist->level = level;
    }
    
    x =  (OS_SKIPLIST_NODE *) OSMemGet(&OS_SkipListPartition,&err);
    x->period = period;
    x->next_time=OSTickCtr+period*1000; 
    x->p_tcb = p_tcb;
    
    for(i=0;i< OS_SKIPLIST_MAX_LEVEL; i++)
     { x->forward[i] = (OS_SKIPLIST_NODE *) 0;
       x->forward[i]->period=INT_MAX;
       x->forward[i]->next_time=INT_MAX;
     }
    
    for (i = 1; i <= level; i++) {
        x->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = x;
    }
    
    
} 



/*******************************************Restructure Skip List***********************************/
void OS_SkipListRestructure(void)

{
    OS_ERR  err;
    OS_SKIPLIST *p_skiplist = &OS_SkipList;
    OS_SKIPLIST_NODE *x=p_skiplist->header->forward[1];
    OS_SKIPLIST_NODE *update[OS_SKIPLIST_MAX_LEVEL+1];
    OS_TCB *p_tcb = x->p_tcb;
    OS_TICK period = x->period;
    OS_TICK next_time = x->next_time;
    next_time += period*1000;
    x->next_time=next_time;
    CPU_INT08U i, level;
                                                 /*Remove the top node*/
    for (i=1; i <= p_skiplist->level; i++)
    {
            if (p_skiplist->header->forward[i] == (OS_SKIPLIST_NODE *)0)
                    break;
            p_skiplist->header->forward[i] = x->forward[i];
    }
    
    OSMemPut(&OS_SkipListPartition,
             (void *) x,
             &err);
    
                                                /*Insert the previous top node
                                                  back into the skip list
                                                  based on the next release time*/
    OS_SkipListInsertNextTime(p_tcb,period,next_time);
}
    
/*******************************************Delete a node from the skip list************************/    
void          OS_SkipListDelete         (OS_SKIPLIST_NODE *node)
{
  CPU_INT08U i;
  OS_SKIPLIST_NODE *update[OS_SKIPLIST_MAX_LEVEL+1];
  OS_SKIPLIST *p_skiplist = &OS_SkipList;
  OS_SKIPLIST_NODE *x = p_skiplist->header;
  for (i = p_skiplist->level; i >= 1; i--) {
      while ((x->forward[i]!= (OS_SKIPLIST_NODE *) 0)&&(x->forward[i]->period < node->period))
          x = x->forward[i];
      update[i] = x;
  }

  x = x->forward[1];
  if (x->period == node->period) {
      for (i = 1; i <= p_skiplist->level; i++) {
          if (update[i]->forward[i] != x)
              break;
          update[i]->forward[i] = x->forward[i];
      }
      OS_SkipListNodeFree(x);
      while (p_skiplist->level > 1 && p_skiplist->header->forward[p_skiplist->level]
              == p_skiplist->header)
          p_skiplist->level--;
      
  }
    
}

/*******************************************Free the memory allocated to the node**********************/
void OS_SkipListNodeFree (OS_SKIPLIST_NODE *node)
{ OS_ERR err;
  CPU_INT08U i;
  if (node != (OS_SKIPLIST_NODE *) 0)
  { for( i = 0; i< OS_SKIPLIST_MAX_LEVEL ; i++)
    { if(node->forward[i]!=(OS_SKIPLIST_NODE *)0)
      OSMemPut(&OS_SkipListPartition,
             (void *) node->forward[i],
             &err);
    }    
    OSMemPut(&OS_SkipListPartition,
             (void *) node,
             &err);
  }
 
}

/*******************************Insert task into the skip list based on the next release time*********/

void          OS_SkipListInsertNextTime (OS_TCB *p_tcb, OS_TICK period, OS_TICK next_time)
{
    OS_ERR err;
    OS_SKIPLIST_NODE *update[OS_SKIPLIST_MAX_LEVEL+1];
    CPU_INT08U i, level;
    OS_SKIPLIST *p_skiplist = &OS_SkipList;
    OS_SKIPLIST_NODE *x = p_skiplist->header;

    for (i = p_skiplist->level; i >= 1; i--) {
        while ((x->forward[i]!= (OS_SKIPLIST_NODE *) 0)&&(x->forward[i]->next_time < next_time))
            x = x->forward[i];
        update[i] = x;
    }
    x = x->forward[1];

    level = OS_SkipListRandLevel();
    if (level > p_skiplist->level) {
        for (i = p_skiplist->level+1; i <= level; i++) {
            update[i] = p_skiplist->header;
        }
        p_skiplist->level = level;
    }

    x =  (OS_SKIPLIST_NODE *) OSMemGet(&OS_SkipListPartition,&err);
    x->period = period;
    x->next_time=next_time;
    x->p_tcb = p_tcb;
    
    for(i=0;i< OS_SKIPLIST_MAX_LEVEL; i++)
     { x->forward[i] = (OS_SKIPLIST_NODE *) 0;
       x->forward[i]->period=INT_MAX;
       x->forward[i]->next_time=INT_MAX;
     }
    
    for (i = 1; i <= level; i++) {
        x->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = x;
    }
 
    
}
    
  