#include <os.h>

CPU_INT08U OS_PCP_Pend(OS_MUTEX *p_mutex, CPU_TS *p_ts)
{
  if ((OSTCBCurPtr->Prio < OS_PCP_System_Ceiling) || (OS_ResourceStackFind(OSTCBCurPtr) == 1u))
    {if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) {    /* Resource available?                                    */
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }
        
        
        OS_PCP_System_Ceiling = OS_PCP_System_Ceiling <= p_mutex->RecCeil ? OS_PCP_System_Ceiling:p_mutex->RecCeil;
        OS_ResourceStackPush(p_mutex,p_mutex->OwnerTCBPtr,p_mutex->RecCeil);  
        
      
        
        return 1;
     }
     
     if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        
        return 2;
    }
    }
}
void OS_PCP_PendAfterPost(OS_MUTEX *p_mutex, OS_TCB *p_tcb)
{
  p_mutex->OwnerTCBPtr       = p_tcb;                     /* Give mutex to new owner                                */
  p_mutex->OwnerOriginalPrio = p_tcb->Prio;
  p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
  
  OS_PCP_System_Ceiling = OS_PCP_System_Ceiling <= p_mutex->RecCeil ? OS_PCP_System_Ceiling:p_mutex->RecCeil;
  OS_ResourceStackPush(p_mutex,p_mutex->OwnerTCBPtr,p_mutex->RecCeil);  
  
}
  
void OS_PCP_Post(void)
{
  OS_ResourceStackPop();
  OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  
  if(p_stack->NbrEntr == 0u)
  {  OS_PCP_System_Ceiling = 19;
     return;
  }
  
  OS_PCP_System_Ceiling = OS_ResourceStackMaxPrio();
  
}
  
  