#include <os.h>

void OS_ResourceStackInit(void)
{
  OS_ERR err;
  OSMemCreate((OS_MEM    *) &OS_ResourceStackPartition,
              (CPU_CHAR  *) "OS Resource Stack Partition",
              (void      *) &OS_ResourceStackNode_Buffer[0][0],      
              (OS_MEM_QTY ) 20,                       
              (OS_MEM_SIZE) sizeof(OS_RESOURCE_STACK_NODE),     
              (OS_ERR    *) &err);
  
  OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  p_stack->Top = (OS_RESOURCE_STACK_NODE *) OSMemGet((OS_MEM *) &OS_ResourceStackPartition, (OS_ERR *) &err);
  p_stack->Top->p_mutex = (OS_MUTEX *) 0;
  p_stack->Top->p_tcb = (OS_TCB *) 0;
  p_stack->Top->next = (OS_RESOURCE_STACK_NODE *) 0;

}

void OS_ResourceStackPush(OS_MUTEX *p_mutex, OS_TCB *p_tcb, OS_PRIO rc)
{ OS_ERR err;
  OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  OS_RESOURCE_STACK_NODE *tmp = (OS_RESOURCE_STACK_NODE *) OSMemGet((OS_MEM *) &OS_ResourceStackPartition, (OS_ERR *) &err);
  tmp->p_mutex = p_mutex;
  tmp->p_tcb = p_tcb;
  tmp->resource_ceiling = rc;
  tmp->next = p_stack->Top;
  p_stack->Top = tmp;
  p_stack->NbrEntr++;

}

void OS_ResourceStackPop(void)
{ OS_ERR err;
  OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  OS_RESOURCE_STACK_NODE *tmp = p_stack->Top;
  p_stack->Top = p_stack->Top->next;
  OSMemPut(&OS_ResourceStackPartition,(void *) tmp, &err);
  p_stack->NbrEntr--;
}

CPU_INT08U OS_ResourceStackFind(OS_TCB *p_tcb)
{ OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  if(p_stack->NbrEntr == 0u)
    return 1;
  
  OS_RESOURCE_STACK_NODE *tmp = p_stack->Top;
  while(tmp->p_tcb != (OS_TCB *)0)
  {
    if(tmp->p_tcb == p_tcb)
      return 1;
    tmp=tmp->next;
   
  }
  
  return 0;
}

OS_PRIO OS_ResourceStackMaxPrio( void )
{
  OS_RESOURCE_STACK *p_stack = &OS_ResourceStack;
  if(p_stack->NbrEntr == 0u)
    return 19;
  
  OS_RESOURCE_STACK_NODE *tmp = p_stack->Top;
  OS_PRIO max = tmp->resource_ceiling;
  
  while(tmp->p_tcb != (OS_TCB *)0)
  { max = max < tmp->resource_ceiling ? max:tmp->resource_ceiling;
    tmp = tmp->next;
    
  }
  
  return max;
  
}
  