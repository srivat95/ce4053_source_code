#include <os.h>
#include <limits.h>
#include <stdlib.h>
//CPU_DATA   OSPrioTbl[OS_PRIO_TBL_SIZE];
void          OS_AVL_Tree_Init          (void)
{ OS_ERR err; 
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;
  p_avltree->root=(OS_AVL_TREE_NODE *) OSMemGet(&OS_AVLTree_Partition,&err);
  p_avltree->root->p_tcb=(OS_TCB *)0;
  p_avltree->root->period=INT_MAX;
  p_avltree->root->prio=INT_MAX;
  p_avltree->root->left = (OS_AVL_TREE_NODE *) 0;
  p_avltree->root->right = (OS_AVL_TREE_NODE *) 0;
  
}

OS_AVL_TREE_NODE *OS_AVL_Tree_Create_Node  (void) 
{ OS_ERR err; 
  OS_AVL_TREE_NODE *node = (OS_AVL_TREE_NODE *) OSMemGet(&OS_AVLTree_Partition,&err);
  node->p_tcb=(OS_TCB *)0;
  node->period=INT_MAX;
  node->prio=INT_MAX;
  node->left = (OS_AVL_TREE_NODE *) 0;
  node->right = (OS_AVL_TREE_NODE *) 0;

  return node;	
}

CPU_INT08U    OS_AVL_Tree_Node_Height   (OS_AVL_TREE_NODE *node )
{
  CPU_INT08U height_left = 0u;
  CPU_INT08U height_right = 0u;
 
  if( node->left ) height_left = OS_AVL_Tree_Node_Height( node->left );
  if( node->right ) height_right = OS_AVL_Tree_Node_Height( node->right );

  return height_right > height_left ? ++height_right : ++height_left;

}

CPU_INT08S    OS_AVL_Tree_Balance_Factor   (OS_AVL_TREE_NODE *node )
{
  CPU_INT08S bf = 0;

  if( node->left  ) bf += OS_AVL_Tree_Node_Height( node->left );
  if( node->right ) bf -= OS_AVL_Tree_Node_Height( node->right );

  return bf ;

}

OS_AVL_TREE_NODE *OS_AVL_Rotate_LeftLeft( OS_AVL_TREE_NODE *node )
{
  OS_AVL_TREE_NODE *a = node;
  OS_AVL_TREE_NODE *b = a->left;
	
  a->left = b->right;
  b->right = a;

  return( b );

}

OS_AVL_TREE_NODE *OS_AVL_Rotate_LeftRight( OS_AVL_TREE_NODE *node )
{
  OS_AVL_TREE_NODE *a = node;
  OS_AVL_TREE_NODE *b = a->left;
  OS_AVL_TREE_NODE *c = b->right;
	
  a->left = c->right;
  b->right = c->left; 
  c->left = b;
  c->right = a;

  return( c );

}

OS_AVL_TREE_NODE *OS_AVL_Rotate_RightLeft( OS_AVL_TREE_NODE *node )
{
  OS_AVL_TREE_NODE *a = node;
  OS_AVL_TREE_NODE *b = a->right;
  OS_AVL_TREE_NODE *c = b->left;	
  
  a->right = c->left;
  b->left = c->right; 
  c->right = b;
  c->left = a;

  return( c );

}
  
OS_AVL_TREE_NODE *OS_AVL_Rotate_RightRight( OS_AVL_TREE_NODE *node )
{
  OS_AVL_TREE_NODE *a = node;
  OS_AVL_TREE_NODE *b = a->right;
	
  a->right = b->left;
  b->left = a;

  return( b );

}

OS_AVL_TREE_NODE *OS_AVL_Balance_Node( OS_AVL_TREE_NODE *node )
{
  OS_AVL_TREE_NODE *newroot = (OS_AVL_TREE_NODE *)0;

	/* Balance our children, if they exist. */
  if( node->left )
  	node->left  = OS_AVL_Balance_Node( node->left  );
  if( node->right ) 
	node->right = OS_AVL_Balance_Node( node->right );

  int bf = OS_AVL_Tree_Balance_Factor( node );

  if( bf >= (CPU_INT08S) 2 ) {
		/* Left Heavy */	

  	if( OS_AVL_Tree_Balance_Factor( node->left ) <= (CPU_INT08S) -1) 
		newroot = OS_AVL_Rotate_LeftRight( node );
	else 
		newroot = OS_AVL_Rotate_LeftLeft( node );

  } else if( bf <= (CPU_INT08S) -2 ) {
		/* Right Heavy */

	if( OS_AVL_Tree_Balance_Factor( node->right ) >= (CPU_INT08S) 1 )
		newroot = OS_AVL_Rotate_RightLeft( node );
	else 
		newroot = OS_AVL_Rotate_RightRight( node );

  } else {
		/* This node is balanced -- no change. */

	newroot = node;
  }

  return( newroot );

} 

void OS_AVL_Tree_Balance( void ) 
{ 
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;
  OS_AVL_TREE_NODE *newroot = (OS_AVL_TREE_NODE *) 0;

  newroot = OS_AVL_Balance_Node( p_avltree->root );

  if( newroot != p_avltree->root )  {
	p_avltree->root = newroot; 
  }

}

/* Insert a new node. */
void OS_AVL_Tree_Insert( OS_TCB *p_tcb, OS_TICK period, OS_PRIO prio ) {
	OS_AVL_TREE_NODE *node; //= (OS_AVL_TREE_NODE *)0;
	OS_AVL_TREE_NODE *next; //= (OS_AVL_TREE_NODE *)0;
	OS_AVL_TREE_NODE *last; //= (OS_AVL_TREE_NODE *)0;
        
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;

	/* Well, there must be a first case */ 	
	if( p_avltree->root->p_tcb == (OS_TCB *)0 ) {
		node = OS_AVL_Tree_Create_Node();
		node->period = period;
    node->p_tcb = p_tcb;
    node->prio  = prio;

		p_avltree->root = node;

	/* Okay.  We have a root already.  Where do we put this? */
	} else {
		next = p_avltree->root;

		while( next != (OS_AVL_TREE_NODE *)0 ) {
			last = next;

			if( period < next->period ) {
                          
				next = next->left;

			} else if( period > next->period ) {
                           
				next = next->right;

			/* Have we already inserted this node? */
			} else if( period == next->period ) {
				
                               period = period+1;
			}
		}

		node = OS_AVL_Tree_Create_Node();
                node->period = period;
                node->p_tcb = p_tcb;
                node->prio  = prio;

		if( period < last->period ) last->left = node;
		if( period > last->period ) last->right = node;
		
	}

	OS_AVL_Tree_Balance();
}

void OS_AVL_Tree_Insert_Prio( OS_TCB *p_tcb, OS_TICK period, OS_PRIO prio ) {
	OS_AVL_TREE_NODE *node;// = (OS_AVL_TREE_NODE *)0;
	OS_AVL_TREE_NODE *next;// = (OS_AVL_TREE_NODE *)0;
	OS_AVL_TREE_NODE *last;// = (OS_AVL_TREE_NODE *)0;
        
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;

	/* Well, there must be a first case */ 	
	if( p_avltree->root->p_tcb == (OS_TCB *)0 ) {
		node = OS_AVL_Tree_Create_Node();
		node->period = period;
    node->p_tcb = p_tcb;
    node->prio  = prio;

		p_avltree->root = node;

	/* Okay.  We have a root already.  Where do we put this? */
	} else {
		next = p_avltree->root;

		while( next != (OS_AVL_TREE_NODE *)0 ) {
			last = next;

			if( prio < next->prio ) {
                          
				next = next->left;

			} else if( prio > next->prio ) {
                           
				next = next->right;

			/* Have we already inserted this node? */
			} else if( prio == next->prio ) {
				/* This shouldn't happen. */
                               //period = period+1;
                             return;
			}
		}

		node = OS_AVL_Tree_Create_Node();
                node->period = period;
                node->p_tcb = p_tcb;
                node->prio  = prio;
                OS_PrioInsert(prio);
		if( prio < last->prio ) last->left = node;
		if( prio > last->prio ) last->right = node;
		
	}

	OS_AVL_Tree_Balance();
}

OS_AVL_TREE_NODE *OS_AVL_Find( OS_PRIO prio ) {
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;

  OS_AVL_TREE_NODE *current = p_avltree->root;

  while( current && current->prio != prio ) {
    if( prio > current->prio )
      current = current->right;
    else
      current = current->left;
  }

  return current;
}



void OS_AVL_Traverse_Node_DFS( OS_AVL_TREE_NODE *node, CPU_INT08U depth, OS_PRIO p ) {
  static OS_PRIO prio;
  
  if (p!=(OS_PRIO) 0)
    prio = p;

  if( node->left ) OS_AVL_Traverse_Node_DFS( node->left, depth + 2 , 0);

  //for( i = 0; i < depth; i++ ) putchar( ' ' );
  //printf( "%d: %d\n", node->value, OS_AVL_Traverse_Node_DFS( node ) );
  //if((node->p_tcb != (OS_TCB     *)&OSTickTaskTCB) && (node->p_tcb != (OS_TCB     *)&OSTmrTaskTCB) && (node->p_tcb != (OS_TCB     *)&AppTaskStartTCB) ){
  if(node->prio >= (OS_PRIO) 5u){  
    node->prio=prio++;
    node->p_tcb->Prio=node->prio;
    //OS_PrioInsert(node->p_tcb->Prio);
  }

  if( node->right ) OS_AVL_Traverse_Node_DFS( node->right, depth + 2 ,0 );
}


void OS_AVL_Traverse_DFS( void ) {
  OS_AVL_TREE *p_avltree = &OS_AVL_Tree;
  //OS_PRIO pri = OS_PrioGetHighest();
  OS_AVL_Traverse_Node_DFS( p_avltree->root, 0 ,11);
}

OS_AVL_TREE_NODE *OS_AVL_MinValueNode(OS_AVL_TREE_NODE *node)
{
    OS_AVL_TREE_NODE *current = node;
 
    /* loop down to find the leftmost leaf */
    while (current->left != (OS_AVL_TREE_NODE *) 0)
        current = current->left;
 
    return current;
}

OS_AVL_TREE_NODE *OS_AVL_Tree_Delete_Node(OS_AVL_TREE_NODE *root,OS_PRIO prio)
{
  OS_ERR err;
  if(root == (OS_AVL_TREE_NODE *) 0)
    return root;
  OS_PrioRemove(prio);
  if ( prio < root->prio )
    root->left = OS_AVL_Tree_Delete_Node(root->left, prio);
 
  // If the key to be deleted is greater than the
  // root's key, then it lies in right subtree
  else if( prio > root->prio )
    root->right = OS_AVL_Tree_Delete_Node(root->right, prio);
 
  // if key is same as root's key, then This is
  // the node to be deleted
  else
  {
    // node with only one child or no child
    if( (root->left == (OS_AVL_TREE_NODE *) 0) || (root->right == (OS_AVL_TREE_NODE *) 0) )
    {
        OS_AVL_TREE_NODE *temp = (root->left == (OS_AVL_TREE_NODE *) 0) ? root->right :root->left;
 
        // No child case
        if (temp == (OS_AVL_TREE_NODE *) 0)
        {
            temp = root;
            root = (OS_AVL_TREE_NODE *) 0;
        }
        else // One child case
            *root = *temp; // Copy the contents of
                            // the non-empty child
            OSMemPut(&OS_AVLTree_Partition,(void *) temp, &err);
        }
     else
     {
            // node with two children: Get the inorder
            // successor (smallest in the right subtree)
            OS_AVL_TREE_NODE *temp = OS_AVL_MinValueNode(root->right);
 
            // Copy the inorder successor's data to this node
            root->prio = temp->prio;
            root->p_tcb = temp->p_tcb;
            root->period = temp->period;
            OSMemPut(&OS_AVLTree_Partition,(void *) temp, &err);
            // Delete the inorder successor
            root->right = OS_AVL_Tree_Delete_Node(root->right, root->prio);
     }
    }
 
    // If the tree had only one node then return
    if (root == (OS_AVL_TREE_NODE *) 0)
      return root;
  
    OS_AVL_TREE_NODE *newroot = (OS_AVL_TREE_NODE *)0;
    
    newroot = OS_AVL_Balance_Node(root);
    
    if(newroot != (OS_AVL_TREE_NODE *) 0)
      return newroot;
    
    return root;
}

void OS_AVL_Tree_Prio_Insert( OS_AVL_TREE_NODE *node, CPU_INT08U depth )
{
  if( node->left ) OS_AVL_Tree_Prio_Insert( node->left, depth + 2 );

  if((node!=(OS_AVL_TREE_NODE *) 0)&&(node->prio >= (OS_PRIO) 5u)){ 
    OS_ERR p_err;
    CPU_SR_ALLOC();
    OS_TCB *newTCB2=(OS_TCB *) OSMemGet(&OS_SkipListPartition2,&p_err); 
    CPU_STK *newSTK2 = (CPU_STK *) OSMemGet(&OS_STK_Partition,&p_err);
    OS_TaskInitTCB(newTCB2);
    OS_TCB *p_tcb=node->p_tcb;
    newSTK2 = OSTaskStkInit(p_tcb->TaskEntryAddr,
                         p_tcb->TaskEntryArg,
                         p_tcb->StkBasePtr,
                         p_tcb->StkLimitPtr,
                         p_tcb->StkSize,
                         p_tcb->Opt);
    
    newTCB2->TaskEntryAddr = p_tcb->TaskEntryAddr;                          
    newTCB2->TaskEntryArg  = p_tcb->TaskEntryArg;                           
    newTCB2->NamePtr       = p_tcb->NamePtr;                          

    newTCB2->Prio          = p_tcb->Prio;                            

    newTCB2->StkPtr        = newSTK2;                            
    newTCB2->StkLimitPtr   = p_tcb->StkLimitPtr;                     

    newTCB2->TimeQuanta    = p_tcb->TimeQuanta;                     
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (p_tcb->TimeQuanta == (OS_TICK)0) {
        newTCB2->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        newTCB2->TimeQuantaCtr = p_tcb->TimeQuanta;
    }
#endif
    newTCB2->ExtPtr        = p_tcb->ExtPtr;                           
    newTCB2->StkBasePtr    = p_tcb->StkBasePtr;                      
    newTCB2->StkSize       = p_tcb->StkSize;                        
    newTCB2->Opt           = p_tcb->Opt;                                                                   

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        newTCB2->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&newTCB2->MsgQ,                                              
                (OS_MSG_QTY) 0u);
#endif
    
    OS_CRITICAL_ENTER();
    CPU_TS startime=OS_TS_GET();
    CPU_TS startime2=OS_TS_GET(); 
    OS_PrioInsert(node->prio);
    OS_RdyListInsertTail(newTCB2);
    OverheadValue = startime - startime2 - startime2 + OS_TS_GET () ;
    OS_CRITICAL_EXIT();
  }

  if( node->right ) OS_AVL_Tree_Prio_Insert( node->right, depth + 2 );
  
}
  
  
  