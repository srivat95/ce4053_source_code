#include <os.h>

void OS_RedBlackTreeInit(void)
{
    OS_ERR  err;
    OSMemCreate((OS_MEM    *) &OS_RedBlackTree_Partition,
                (CPU_CHAR  *) "OS Red Black Tree Partition",
                (void      *) &OS_RedBlackNode_Buffer[0][0],      
                (OS_MEM_QTY ) 20,                       
                (OS_MEM_SIZE) sizeof(OS_RED_BLACK_NODE),     
                (OS_ERR    *) &err);

    
    /*OSRedBlackTree                  = (OS_RED_BLACK_TREE  *) OSMemGet((OS_MEM *) &OSRedBlackTreePartition, 
                                                                      (OS_ERR *) &err); 
    */                                                                
    OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OSRedBlackTree->RootNode        = (OS_RED_BLACK_NODE  *) OSMemGet((OS_MEM *) &OS_RedBlackTree_Partition, 
                                                                      (OS_ERR *) &err);
    
    OSRedBlackTree->NilNode         = (OS_RED_BLACK_NODE  *) OSMemGet((OS_MEM *) &OS_RedBlackTree_Partition, 
                                                                      (OS_ERR *) &err);
    
    OSRedBlackTree->NilNode->Parent      = OSRedBlackTree->NilNode;
    OSRedBlackTree->NilNode->LeftChild   = OSRedBlackTree->NilNode;
    OSRedBlackTree->NilNode->RightChild  = OSRedBlackTree->NilNode;
    OSRedBlackTree->NilNode->TaskPtr     = (OS_TCB *) 0;    
    OSRedBlackTree->NilNode->mutex       = (OS_MUTEX *) 0;
    OSRedBlackTree->NilNode->Colour      = (OS_RED_BLACK_NODE_CLR) 0;                               /* Nil node is always black */
    
    OSRedBlackTree->RootNode->TaskPtr     = (OS_TCB *) 0;                                            
    OSRedBlackTree->RootNode->Colour      = (OS_RED_BLACK_NODE_CLR) 0;                               /* Root node is always black */
    OSRedBlackTree->RootNode->prio        = (OS_PRIO) 0;
    OSRedBlackTree->RootNode->mutex       = (OS_MUTEX *) 0;
    OSRedBlackTree->RootNode->Parent      = OSRedBlackTree->NilNode;
    OSRedBlackTree->RootNode->LeftChild   = OSRedBlackTree->NilNode;
    OSRedBlackTree->RootNode->RightChild  = OSRedBlackTree->NilNode;
    
    OSRedBlackRootNode              = (OS_RED_BLACK_NODE  *) OSMemGet((OS_MEM *) &OS_RedBlackTree_Partition, 
                                                                      (OS_ERR *) &err);
    OSRedBlackRootNode->TaskPtr     = (OS_TCB *) 0;                                            
    OSRedBlackRootNode->Colour      = (OS_RED_BLACK_NODE_CLR) 0;                               /* Root node is always black */
    OSRedBlackRootNode->prio        = (OS_PRIO) 0;
    OSRedBlackRootNode->mutex       = (OS_MUTEX *) 0;
    OSRedBlackRootNode->Parent      = (OS_RED_BLACK_NODE  *) 0;
    OSRedBlackRootNode->LeftChild   = (OS_RED_BLACK_NODE  *) 0;
    OSRedBlackRootNode->RightChild  = (OS_RED_BLACK_NODE  *) 0;
}


void OS_RedBlackTreeLeftRotate  (OS_RED_BLACK_NODE *RBTNodeLeft)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *RightChildOfNode;
    OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
    RightChildOfNode = RBTNodeLeft->RightChild; 
    RBTNodeLeft->RightChild = RightChildOfNode->LeftChild;

    if (RightChildOfNode->LeftChild != Nil)
        RightChildOfNode->LeftChild->Parent = RBTNodeLeft;
    
    RightChildOfNode->Parent = RBTNodeLeft->Parent;   

    if(RBTNodeLeft == RBTNodeLeft->Parent->LeftChild)
        RBTNodeLeft->Parent->LeftChild = RightChildOfNode;
    else
        RBTNodeLeft->Parent->RightChild = RightChildOfNode;
    
    RightChildOfNode->LeftChild = RBTNodeLeft;
    RBTNodeLeft->Parent = RightChildOfNode;
}

void OS_RedBlackTreeRightRotate (OS_RED_BLACK_NODE *RBTNodeRight)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *LeftChildOfNode;
    OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
    LeftChildOfNode = RBTNodeRight->LeftChild;
    RBTNodeRight->LeftChild = LeftChildOfNode->RightChild;

    if (LeftChildOfNode->RightChild != Nil) 
        LeftChildOfNode->RightChild->Parent = RBTNodeRight;
    
    LeftChildOfNode->Parent = RBTNodeRight->Parent;
    
    if(RBTNodeRight == RBTNodeRight->Parent->LeftChild) 
      RBTNodeRight->Parent->LeftChild = LeftChildOfNode;
    else
      RBTNodeRight->Parent->RightChild = LeftChildOfNode;

    LeftChildOfNode->RightChild = RBTNodeRight;
    RBTNodeRight->Parent = LeftChildOfNode;
}

// Inserts RBTNode into the tree as if it were a regular binary tree
void OS_RedBlackTreeInsertHelp  (OS_RED_BLACK_NODE* RBTNode)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *x, *y;
    OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
    
    RBTNode->LeftChild   = Nil;
    RBTNode->RightChild  = Nil; 
    y = OSRedBlackTree->RootNode;
    x = OSRedBlackTree->RootNode->LeftChild;
    while(x != Nil)
    {
        y = x;
        if (x->prio > RBTNode->prio)
            x = x->LeftChild;
        else
            x = x->RightChild;
    }
    RBTNode->Parent = y;
    if ((y == OSRedBlackTree->RootNode) || (y->prio > RBTNode->prio)) 
    {
        y->LeftChild = RBTNode;
    } 
    else 
    {
        y->RightChild = RBTNode;
    }
}

void OS_RedBlackTreeInsert      (OS_TCB *p_tcb,
                                 OS_PRIO prio, OS_MUTEX *mutex)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *NewNode, *UncleOfNewNode;
    OS_ERR  err;
    OS_RED_BLACK_NODE *RBTNewNode;
 
    NewNode              = (OS_RED_BLACK_NODE  *) OSMemGet((OS_MEM *) &OS_RedBlackTree_Partition, 
                                                              (OS_ERR *) &err);
    NewNode->TaskPtr     = p_tcb;
    //NewNode->TaskPtrCopy = p_tcb_rdy_list;
    NewNode->prio  = prio;
    NewNode->mutex = mutex;
    OS_RedBlackTreeInsertHelp(NewNode);
    RBTNewNode           = NewNode;
    NewNode->Colour      = (OS_RED_BLACK_NODE_CLR) 1;                       /* 1 means red, 0 means black          */                                                                 
    while(NewNode->Parent->Colour == (OS_RED_BLACK_NODE_CLR) 1)
    {
        if(NewNode->Parent == NewNode->Parent->Parent->LeftChild)           /* Parent of NewNode is a Left child   */
        {
            UncleOfNewNode = NewNode->Parent->Parent->RightChild;
            if(UncleOfNewNode->Colour == (OS_RED_BLACK_NODE_CLR) 1)         /* UncleOfNewNode is Red               */
            {
                NewNode->Parent->Colour         = (OS_RED_BLACK_NODE_CLR) 0;
                UncleOfNewNode->Colour          = (OS_RED_BLACK_NODE_CLR) 0;
                NewNode->Parent->Parent->Colour = (OS_RED_BLACK_NODE_CLR) 1;
                NewNode                         =  NewNode->Parent->Parent;
            }   
            else                                                            /* UncleOfNewNode is Black             */                    
            {
                if(NewNode == NewNode->Parent->RightChild)                  /* NewNode is a Right child            */
                {
                    NewNode = NewNode->Parent;
                    OS_RedBlackTreeLeftRotate(NewNode);
                }
                NewNode->Parent->Colour           = (OS_RED_BLACK_NODE_CLR) 0;
                NewNode->Parent->Parent->Colour   = (OS_RED_BLACK_NODE_CLR) 1;
                OS_RedBlackTreeRightRotate(NewNode->Parent->Parent);
            }
        }
        else                                                               /* Parent of NewNode is a Right child  */
        {
            UncleOfNewNode = NewNode->Parent->Parent->LeftChild;
            if(UncleOfNewNode->Colour == (OS_RED_BLACK_NODE_CLR) 1)        /* UncleOfNewNode is Red               */     
            {
                NewNode->Parent->Colour         = (OS_RED_BLACK_NODE_CLR) 0;
                UncleOfNewNode->Colour          = (OS_RED_BLACK_NODE_CLR) 0;
                NewNode->Parent->Parent->Colour = (OS_RED_BLACK_NODE_CLR) 1;
                NewNode                         =  NewNode->Parent->Parent;
            }
            else                                                           /* UncleOfNewNode is Black             */     
            {
                if(NewNode == NewNode->Parent->LeftChild)                  /* NewNode is a Left child             */
                {
                    NewNode = NewNode->Parent;
                    OS_RedBlackTreeRightRotate(NewNode);
                }
                NewNode->Parent->Colour           = (OS_RED_BLACK_NODE_CLR) 0;
                NewNode->Parent->Parent->Colour   = (OS_RED_BLACK_NODE_CLR) 1;
                OS_RedBlackTreeLeftRotate(NewNode->Parent->Parent);
            }
        } 
    }
    OSRedBlackTree->RootNode->LeftChild->Colour = (OS_RED_BLACK_NODE_CLR) 0;
}


void OS_RedBlackTreeRemove      (OS_RED_BLACK_NODE *z)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *x, *y;
    OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
    
    if ((z->LeftChild == Nil) || (z->RightChild == Nil))
        y = z;
    else
        y = OS_RedBlackTreeSuccessor(z);

    if (y->LeftChild == Nil)
        x = y->RightChild;
    else
        x = y->LeftChild;
    
    x->Parent = y->Parent;
    if (OSRedBlackTree->RootNode == x->Parent)    
        OSRedBlackTree->RootNode->LeftChild = x;
    else
    {
        if (y == y->Parent->LeftChild) 
        {
            y->Parent->LeftChild  = x;
        } 
        else
        {
            y->Parent->RightChild = x;
        }
    }
    if (y != z) // y should not be nil in this case 
    { 
        // y is the node to splice out and x is its child
        if (y->Colour == (OS_RED_BLACK_NODE_CLR) 0)
            OS_RedBlackTreeRemoveFix(x);
        
        y->LeftChild  = z->LeftChild;
        y->RightChild = z->RightChild;
        y->Parent     = z->Parent;
        y->Colour     = z->Colour;
        z->LeftChild->Parent  = y;
        z->RightChild->Parent = y;
        if (z == z->Parent->LeftChild) 
            z->Parent->LeftChild  = y; 
        else 
            z->Parent->RightChild = y;
    } 
    else 
    {
        if (y->Colour == (OS_RED_BLACK_NODE_CLR) 0)
            OS_RedBlackTreeRemoveFix(x);
    }
}

void OS_RedBlackTreeRemoveFix  (OS_RED_BLACK_NODE *RBTNode) 
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    //rb_red_blk_node* root=tree->root->LeftChild;
    OS_RED_BLACK_NODE *w;
    int BlackNode = 0;
    while( (!RBTNode->Colour) && (OSRedBlackTree->RootNode != RBTNode)) {  
      if (RBTNode == RBTNode->Parent->LeftChild) 
      {
        w = RBTNode->Parent->RightChild;
        if (w->Colour) 
        {
            w->Colour = 0;
            RBTNode->Parent->Colour=1;
            //LeftRotate(RBTNode->Parent);
            OS_RedBlackTreeLeftRotate(RBTNode->Parent);
            w = RBTNode->Parent->RightChild;
        }
        if ( (!w->RightChild->Colour) && (!w->LeftChild->Colour) ) 
        { 
          w->Colour = 1;
          RBTNode = RBTNode->Parent;
        } 
        else 
        {
          if (!w->RightChild->Colour) 
          {
            w->LeftChild->Colour=0;
            w->Colour=1;
            //RightRotate(tree,w);
            OS_RedBlackTreeRightRotate(w);
            w=RBTNode->Parent->RightChild;
          }
          w->Colour = RBTNode->Parent->Colour;
          RBTNode->Parent->Colour = 0;
          w->RightChild->Colour   = 0;
          //LeftRotate(tree, RBTNode->Parent);
          OS_RedBlackTreeLeftRotate(RBTNode->Parent);
          RBTNode = OSRedBlackTree->RootNode; /* this is to exit while loop */   
        }
      } 
      else 
      { /* the code below is has left and right switched from above */
        w = RBTNode->Parent->LeftChild;
        if (w->Colour) 
        {
          w->Colour = 0;
          RBTNode->Parent->Colour = 1;
          //RightRotate(tree, RBTNode->Parent);
          OS_RedBlackTreeRightRotate(RBTNode->Parent);
          w = RBTNode->Parent->LeftChild;
        }
        if ((!w->RightChild->Colour) && (!w->LeftChild->Colour)) 
        { 
            w->Colour=1;
            RBTNode = RBTNode->Parent;
        }
        else 
        {
          if (!w->LeftChild->Colour) 
          {
            w->RightChild->Colour=0;
            w->Colour = 1;
            //LeftRotate(tree, w);
            OS_RedBlackTreeLeftRotate(w);
            w = RBTNode->Parent->LeftChild;
          }
          w->Colour = RBTNode->Parent->Colour;
          RBTNode->Parent->Colour = 0;
          w->LeftChild->Colour    = 0;
          //RightRotate(tree, RBTNode->Parent);
          OS_RedBlackTreeLeftRotate(RBTNode->Parent);
          RBTNode = OSRedBlackTree->RootNode; /* this is to exit while loop */  // replace
        }
      }
    }
    RBTNode->Colour = 0;
}

OS_RED_BLACK_NODE*  OS_RedBlackTreeSearch     (CPU_CHAR   *TskName,
                                               OS_PRIO  prio)
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE *RBTNodeToRemove;
    OS_RED_BLACK_NODE *RBTNode = OSRedBlackTree->RootNode->LeftChild;
    int                NodeToDeleteFound = 0;
    
    if (RBTNode == (OS_RED_BLACK_NODE  *) 0) 
        RBTNodeToRemove = (OS_RED_BLACK_NODE  *) 0;
    
    while(NodeToDeleteFound != 1)
    {
        if (RBTNode->prio > prio) 
            RBTNode = RBTNode->LeftChild;
        else if (RBTNode->prio < prio)
            RBTNode = RBTNode->RightChild;
        else
        {
            if(*RBTNode->TaskPtr->NamePtr == *TskName)
                NodeToDeleteFound = 1;
        }
        if (RBTNode == (OS_RED_BLACK_NODE  *) 0)
            RBTNodeToRemove = (OS_RED_BLACK_NODE  *) 0;
    }
    RBTNodeToRemove = RBTNode;
    return RBTNodeToRemove;
}
OS_RED_BLACK_NODE*  OS_RedBlackTreeSuccessor  (OS_RED_BLACK_NODE *RBTNode) 
{   OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
    OS_RED_BLACK_NODE* y;
    OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
    y = RBTNode->RightChild;
    if (y != (OS_RED_BLACK_NODE  *) 0)
    {   
        while(y->LeftChild != Nil) //returns the minium of the right subtree of x
        {  
            y = y->LeftChild;
        }
        return y;
    }
    else 
    {
        y = RBTNode->Parent;
        while(RBTNode == y->RightChild) // sentinel used instead of checking for nil 
        { 
            RBTNode = y;
            y = y->Parent;
        }
        if (y == OSRedBlackTree->RootNode) 
            return (OS_RED_BLACK_NODE  *) 0;
        return y;
    }
}

OS_RED_BLACK_NODE *OS_RedBlackTreeGetHighestPrio( void )
{
  OS_RED_BLACK_TREE *OSRedBlackTree = &OS_RedBlackTree;
  OS_RED_BLACK_NODE *node = OSRedBlackTree->RootNode->LeftChild;
  OS_RED_BLACK_NODE *Nil = OSRedBlackTree->NilNode;
  if(node == (OS_RED_BLACK_NODE *) 0)
    return 0;
  while (node->LeftChild != Nil)
  {
    node=node->LeftChild;
  }
  //OS_TCB *ret = node->TaskPtr;
  //OS_RedBlackTreeRemove(node);
  return node;
  
}
  